package app.fedilab.mobilizon;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.navigation.NavigationView;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import app.fedilab.mobilizon.client.RetrofitMobilizonAPI;
import app.fedilab.mobilizon.client.entities.WellKnownNodeinfo;
import app.fedilab.mobilizon.helper.Helper;
import app.fedilab.mobilizon.webview.MobilizonWebChromeClient;
import app.fedilab.mobilizon.webview.MobilizonWebViewClient;
import es.dmoral.toasty.Toasty;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    public static int PICK_INSTANCE = 5641;
    public static boolean isAuthenticated = false;
    final private int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;
    private final LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(@NonNull final Location location) {
            SharedPreferences sharedpref = getSharedPreferences(Helper.APP_SHARED_PREF, MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedpref.edit();
            editor.putString(Helper.LAST_LOCATION, location.getLongitude() + "," + location.getLatitude() + "," + location.getAltitude());
            editor.apply();
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(@NonNull String provider) {
        }

        @Override
        public void onProviderDisabled(@NonNull String provider) {

        }
    };
    private RelativeLayout progress;
    private WebView main_webview;
    private TextView progressText;
    private  FrameLayout webview_container;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final DrawerLayout drawer = findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        toggle.setDrawerIndicatorEnabled(false);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setHomeButtonEnabled(true);

            toggle = new ActionBarDrawerToggle(
                    this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            toggle.setDrawerIndicatorEnabled(true);
            drawer.addDrawerListener(toggle);
            toggle.syncState();

        }
        final NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        if (Build.VERSION.SDK_INT >= 23) {
            permissionsAPI();
        }

        WebView.setWebContentsDebuggingEnabled(true);
        drawMenu();
        main_webview = findViewById(R.id.main_webview);

        progress = findViewById(R.id.progress);
        progressText = findViewById(R.id.progressText);
        webview_container = findViewById(R.id.webview_container);
        final ViewGroup videoLayout = findViewById(R.id.videoLayout);
        String instance = Helper.getLiveInstance(MainActivity.this);
        Helper.initializeWebview(MainActivity.this, main_webview);
        MobilizonWebChromeClient mobilizonWebChromeClient = new MobilizonWebChromeClient(MainActivity.this, main_webview, webview_container, videoLayout);
        main_webview.setWebChromeClient(mobilizonWebChromeClient);
        main_webview.setWebViewClient(new MobilizonWebViewClient(MainActivity.this));

        main_webview.loadUrl("https://" + instance);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        MenuItem myActionMenuItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) myActionMenuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                try {
                    main_webview.loadUrl("https://" + Helper.getLiveInstance(MainActivity.this) + "/search?term=" + URLEncoder.encode(query, String.valueOf(StandardCharsets.UTF_8)));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                if (!searchView.isIconified()) {
                    searchView.setIconified(true);
                }
                myActionMenuItem.collapseActionView();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_instance) {
            showRadioButtonDialogFullInstances();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void showProgressDialog() {
        if (progress != null) {
            progress.setVisibility(View.VISIBLE);
            webview_container.setVisibility(View.GONE);
        }
    }

    public void hideProgressDialog() {
        if (progress != null) {
            progress.setVisibility(View.GONE);
            webview_container.setVisibility(View.VISIBLE);
        }
    }

    public void setProgressDialog(int progress) {
        if (progressText != null) {
            progressText.setText(String.format("%s%%", progress));
        }
    }

    @Override
    public void onBackPressed() {
        if (main_webview.canGoBack()) {
            main_webview.goBack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.nav_explore) {
            main_webview.loadUrl("https://" + Helper.getLiveInstance(MainActivity.this) + "/search");
        } else if (id == R.id.nav_my_event) {
            main_webview.loadUrl("https://" + Helper.getLiveInstance(MainActivity.this) + "/events/me");
        } else if (id == R.id.nav_my_group) {
            main_webview.loadUrl("https://" + Helper.getLiveInstance(MainActivity.this) + "/groups/me");
        } else if (id == R.id.nav_create_event) {
            main_webview.loadUrl("https://" + Helper.getLiveInstance(MainActivity.this) + "/events/create");
        } else if (id == R.id.nav_login) {
            main_webview.loadUrl("https://" + Helper.getLiveInstance(MainActivity.this) + "/login");
        } else if (id == R.id.nav_logout) {
            Helper.clearCookies(MainActivity.this);
            isAuthenticated = false;
            drawMenu();
            main_webview.loadUrl("https://" + Helper.getLiveInstance(MainActivity.this));
        } else if (id == R.id.nav_profile) {
            main_webview.loadUrl("https://" + Helper.getLiveInstance(MainActivity.this) + "/identity/update");
        } else if (id == R.id.nav_register) {
            main_webview.loadUrl("https://" + Helper.getLiveInstance(MainActivity.this) + "/register/user");
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @SuppressLint("ApplySharedPref")
    private void showRadioButtonDialogFullInstances() {
        final SharedPreferences sharedpreferences = getSharedPreferences(Helper.APP_PREFS, Context.MODE_PRIVATE);
        AlertDialog.Builder alt_bld = new AlertDialog.Builder(this);
        alt_bld.setTitle(R.string.instance_choice);
        String instance = Helper.getLiveInstance(MainActivity.this);
        final EditText input = new EditText(MainActivity.this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);
        alt_bld.setView(input);
        input.setText(instance);
        alt_bld.setPositiveButton(R.string.validate,
                (dialog, which) -> new Thread(() -> {
                    try {
                        String newInstance = input.getText().toString().trim();
                        WellKnownNodeinfo.NodeInfo instanceNodeInfo = new RetrofitMobilizonAPI(MainActivity.this, newInstance).getNodeInfo();
                        if (instanceNodeInfo.getSoftware() != null && instanceNodeInfo.getSoftware().getName().trim().toLowerCase().compareTo("mobilizon") == 0) {
                            SharedPreferences.Editor editor = sharedpreferences.edit();
                            editor.putString(Helper.PREF_INSTANCE, newInstance);
                            editor.commit();
                            runOnUiThread(() -> {
                                dialog.dismiss();
                                main_webview.clearHistory();
                                main_webview.loadUrl("https://" + instance);
                            });
                        } else {
                            runOnUiThread(() -> Toasty.error(MainActivity.this, getString(R.string.not_valide_instance), Toast.LENGTH_LONG).show());
                        }
                    } catch (Exception e) {
                        runOnUiThread(() -> Toasty.error(MainActivity.this, getString(R.string.not_valide_instance), Toast.LENGTH_LONG).show());
                        e.printStackTrace();
                    }

                }).start());
        //TODO: uncomment and add support for picking instances when available
        alt_bld.setNegativeButton(R.string.cancel, (dialog, which) -> dialog.dismiss());
        /*alt_bld.setNeutralButton(R.string.help, (dialog, which) -> {
            startActivityForResult(intent, PICK_INSTANCE);
        });*/
        AlertDialog alert = alt_bld.create();
        alert.show();
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void permissionsAPI() {
        List<String> permissionsNeeded = new ArrayList<>();

        final List<String> permissionsList = new ArrayList<>();
        if (!addPermission(permissionsList))
            permissionsNeeded.add(getString(R.string.show_location));

        if (permissionsList.size() > 0) {
            if (permissionsNeeded.size() > 0) {

                // Need Rationale
                StringBuilder message = new StringBuilder(getString(R.string.access_needed, permissionsNeeded.get(0)));

                for (int i = 1; i < permissionsNeeded.size(); i++)
                    message.append(", ").append(permissionsNeeded.get(i));

                showMessageOKCancel(message.toString(),
                        (dialog, which) -> requestPermissions(permissionsList.toArray(new String[0]),
                                REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS));
                return;
            }
            requestPermissions(permissionsList.toArray(new String[0]),
                    REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(MainActivity.this)
                .setMessage(message)
                .setPositiveButton(getString(R.string.validate), okListener)
                .setNegativeButton(getString(R.string.cancel), null)
                .create()
                .show();
    }

    @TargetApi(Build.VERSION_CODES.M)
    private boolean addPermission(List<String> permissionsList) {

        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(Manifest.permission.ACCESS_FINE_LOCATION);
            // Check for Rationale Option
            return shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION);
        } else {
            LocationManager mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

            assert mLocationManager != null;
            mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, Helper.LOCATION_REFRESH_TIME,
                    Helper.LOCATION_REFRESH_DISTANCE, mLocationListener);
        }
        return true;
    }

    public void drawMenu() {
        final NavigationView navigationView = findViewById(R.id.nav_view);
        if (isAuthenticated) {
            navigationView.getMenu().findItem(R.id.nav_create_event).setVisible(true);
            navigationView.getMenu().findItem(R.id.nav_profile).setVisible(true);
            navigationView.getMenu().findItem(R.id.nav_login).setVisible(false);
            navigationView.getMenu().findItem(R.id.nav_logout).setVisible(true);
            navigationView.getMenu().findItem(R.id.nav_register).setVisible(false);
        } else {
            navigationView.getMenu().findItem(R.id.nav_create_event).setVisible(false);
            navigationView.getMenu().findItem(R.id.nav_profile).setVisible(false);
            navigationView.getMenu().findItem(R.id.nav_login).setVisible(true);
            navigationView.getMenu().findItem(R.id.nav_logout).setVisible(false);
            navigationView.getMenu().findItem(R.id.nav_register).setVisible(true);
        }
        navigationView.invalidate();
    }

}