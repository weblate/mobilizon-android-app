package app.fedilab.mobilizon.client;
/* Copyright 2020 Thomas Schneider
 *
 * This file is a part of Mobilizon app
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * Mobilizon app is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Mobilizon app; if not,
 * see <http://www.gnu.org/licenses>. */

import android.content.Context;

import java.io.IOException;
import java.net.URL;

import app.fedilab.mobilizon.client.entities.WellKnownNodeinfo;
import app.fedilab.mobilizon.helper.Helper;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitMobilizonAPI {


    private String finalUrl;
    private Context _context;
    private String instance;


    public RetrofitMobilizonAPI(Context context) {
        _context = context;
        instance = Helper.getLiveInstance(context);
        finalUrl = "https://" + Helper.getLiveInstance(context) + "/api/v1/";
    }


    public RetrofitMobilizonAPI(Context context, String instance) {
        _context = context;
        this.instance = instance;
        finalUrl = "https://" + instance.toLowerCase() + "/";
    }

    private MobilizonService init() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(finalUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit.create(MobilizonService.class);
    }


    /**
     * Get NodeInfo
     *
     * @return APIResponse
     */
    public WellKnownNodeinfo.NodeInfo getNodeInfo() {
        MobilizonService mobilizonService = init();
        try {
            Call<WellKnownNodeinfo> wellKnownNodeinfoCall = mobilizonService.getWellKnownNodeinfo();
            Response<WellKnownNodeinfo> response = wellKnownNodeinfoCall.execute();
            if (response.isSuccessful() && response.body() != null) {
                int size = response.body().getLinks().size();
                String url = response.body().getLinks().get(size - 1).getHref();
                if (size > 0 && url != null) {
                    mobilizonService = init();
                    String path = new URL(url).getPath();
                    path = path.replaceFirst("/", "").trim();
                    Call<WellKnownNodeinfo.NodeInfo> nodeinfo = mobilizonService.getNodeinfo(path);
                    Response<WellKnownNodeinfo.NodeInfo> responseNodeInfo = nodeinfo.execute();
                    return responseNodeInfo.body();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
