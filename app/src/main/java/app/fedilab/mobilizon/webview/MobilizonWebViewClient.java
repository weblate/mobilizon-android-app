package app.fedilab.mobilizon.webview;
/* Copyright 2020 Thomas Schneider
 *
 * This file is a part of Mobilizon app
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * Mobilizon app is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Mobilizon app; if not,
 * see <http://www.gnu.org/licenses>. */
import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings;
import android.view.View;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.RequiresApi;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.google.android.material.snackbar.Snackbar;

import java.util.Iterator;
import java.util.Map;

import app.fedilab.mobilizon.MainActivity;
import app.fedilab.mobilizon.R;
import app.fedilab.mobilizon.helper.Helper;

public class MobilizonWebViewClient extends WebViewClient {

    private Activity activity;
    private CoordinatorLayout rootView;

    public MobilizonWebViewClient(Activity activity){
        this.activity = activity;
        rootView = activity.findViewById(R.id.main_layout);
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        view.loadUrl(url);
        return true;
    }



    @Override
    public WebResourceResponse shouldInterceptRequest(final WebView view, WebResourceRequest request) {
        if(request.getUrl().toString().endsWith("api")){
            Map<String, String> headers = request.getRequestHeaders();
            Iterator<Map.Entry<String, String>> it = headers.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry<String, String> pair = it.next();
                if( pair.getKey().compareTo("authorization") == 0 && pair.getValue().startsWith("Bearer")) {
                    MainActivity.isAuthenticated = true;
                }
                it.remove();
            }
        }
        return super.shouldInterceptRequest(view, request);
    }

    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon) {
        view.setVisibility(View.INVISIBLE);
        super.onPageStarted(view, url, favicon);
        if (activity instanceof MainActivity) {
            ((MainActivity) activity).showProgressDialog();
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onReceivedError(WebView view, WebResourceRequest request,
                                WebResourceError error) {


        if (!isConnected()) {
            final Snackbar snackBar = Snackbar.make(rootView, activity.getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE);
            snackBar.setAction(activity.getString(R.string.enable_data), v -> {
                activity.startActivityForResult(new Intent(Settings.ACTION_WIRELESS_SETTINGS), 0);
                view.loadUrl("javascript:window.location.reload( true )");
                snackBar.dismiss();
            });
            snackBar.show();
        }

        super.onReceivedError(view, request, error);

    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onReceivedHttpError(WebView view,
                                    WebResourceRequest request, WebResourceResponse errorResponse) {

        if (!isConnected()) {
            final Snackbar snackBar = Snackbar.make(rootView, activity.getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE);
            snackBar.setAction(activity.getString(R.string.enable_data), v -> {
                activity.startActivityForResult(new Intent(Settings.ACTION_WIRELESS_SETTINGS), 0);
                view.loadUrl("javascript:window.location.reload( true )");
                snackBar.dismiss();
            });
            snackBar.show();
        }
        super.onReceivedHttpError(view, request, errorResponse);
    }


    /**
     * Check if there is any connectivity
     *
     * @return is Device Connected
     */
    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    private boolean isConnected() {
        ConnectivityManager cm = (ConnectivityManager)
                activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (null != cm) {
            NetworkInfo info = cm.getActiveNetworkInfo();
            return (info != null && info.isConnected());
        } else {
            return false;
        }
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        Helper.injectCSS(activity, view, "css/style.css");
        ((MainActivity)activity).hideProgressDialog();
        ((MainActivity)activity).drawMenu();
        view.setVisibility(View.VISIBLE);
    }
}
