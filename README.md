# Mobilizon app [![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)
A simple client to interact with Mobilizon instances

Currently the app works with a webview. It supports fullscreen for videos and location. You can switch to another instance. Its validity will be checked with node info.
